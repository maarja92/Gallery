﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Gallery.Models;

namespace Gallery.Controllers
{
    public class FolderLikesController : Controller
    {
        private GalleryContext db = new GalleryContext();

        // GET: FolderLikes
        public ActionResult Index()
        {
            var folderLikes = db.FolderLikes.Include(f => f.Folder);
            return View(folderLikes.ToList());
        }

        // GET: FolderLikes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FolderLike folderLike = db.FolderLikes.Find(id);
            if (folderLike == null)
            {
                return HttpNotFound();
            }
            return View(folderLike);
        }

        // GET: FolderLikes/Create
       
        public ActionResult Create(int? id)
        {
                var folderLike = new FolderLike();
                folderLike.FolderId = (Int32)id;
                db.FolderLikes.Add(folderLike);
                db.SaveChanges();
            return RedirectToAction("Index", "Folder");
        }

        // POST: FolderLikes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,FolderId")] FolderLike folderLike, int folderId)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        folderLike.FolderId = folderId;
        //        db.FolderLikes.Add(folderLike);
        //        db.SaveChanges();
        //        return RedirectToAction("Index", "Folder");
        //    }

        //    ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", folderLike.FolderId);
        //    return View(folderLike);
        //}

        // GET: FolderLikes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FolderLike folderLike = db.FolderLikes.Find(id);
            if (folderLike == null)
            {
                return HttpNotFound();
            }
            ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", folderLike.FolderId);
            return View(folderLike);
        }

        // POST: FolderLikes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FolderId")] FolderLike folderLike)
        {
            if (ModelState.IsValid)
            {
                db.Entry(folderLike).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", folderLike.FolderId);
            return View(folderLike);
        }

        // GET: FolderLikes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FolderLike folderLike = db.FolderLikes.Find(id);
            if (folderLike == null)
            {
                return HttpNotFound();
            }
            return View(folderLike);
        }

        // POST: FolderLikes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FolderLike folderLike = db.FolderLikes.Find(id);
            db.FolderLikes.Remove(folderLike);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
