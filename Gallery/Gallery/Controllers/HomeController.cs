﻿using Gallery.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Gallery.Controllers
{
    public class HomeController : Controller
    {
        private GalleryContext db = new GalleryContext();

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult FileUpload(HttpPostedFileBase file, int PictureID)
        {
            if (file != null)
            {

                // save the image path path to the database or you can send image
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }

            }
            // after successfully uploading redirect the user
            return RedirectToAction("actionname", "controller name");
        }

        public ActionResult Index(string sortOrder)
        {
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            var pictures = db.Pictures.OrderByDescending(s => s.DateAdded).Take(3).ToList();
           
            

            var pictureIds = db.Votes.GroupBy(x => x.PictureId).OrderByDescending(x => x.Count()).Select(x => x.Key).Take(4).ToList();
            List<Picture> mostLikedPictures = new List<Picture>();
            foreach (var pictureId in pictureIds)
            {
                mostLikedPictures.Add(db.Pictures.Find(pictureId));
            }
            ViewBag.MostLikedPictures = mostLikedPictures;



            var randomPicture = db.Pictures.OrderBy(s => s.DateAdded);
            if(randomPicture.ToList().Count > 0)
            {
                Random random = new Random();
                int toSkip = random.Next(0, randomPicture.Count());
                List<Picture> randomPictures = new List<Picture>();


                randomPictures.Add(randomPicture.Skip(toSkip).Take(1).First());
                toSkip = random.Next(0, randomPicture.Count());
                randomPictures.Add(randomPicture.Skip(toSkip).Take(1).First());
                toSkip = random.Next(0, randomPicture.Count());
                randomPictures.Add(randomPicture.Skip(toSkip).Take(1).First());
                ViewBag.RandomPicture = randomPictures;
            }
          

            var showFolders = db.Folders.OrderByDescending(s => s.DateAdded).Take(6).ToList();
            ViewBag.Folders = showFolders;

            return View(pictures);

        }

    }
}