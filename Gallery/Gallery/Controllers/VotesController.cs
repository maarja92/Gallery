﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Gallery.Models;

namespace Gallery.Controllers
{
    public class VotesController : Controller
    {
        private GalleryContext db = new GalleryContext();

        // GET: Votes
        public ActionResult Index()
        {
            var votes = db.Votes.Include(v => v.Picture).Include(v => v.User);
            return View(votes.ToList());
        }

        // GET: Votes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vote vote = db.Votes.Find(id);
            if (vote == null)
            {
                return HttpNotFound();
            }
            return View(vote);
        }

        // GET: Votes/Create
        public ActionResult Create(int id)
        {
            var vote = new Vote();
            vote.PictureId = (Int32)id;   
            db.Votes.Add(vote);
            db.SaveChanges();

            return RedirectToAction("Index", "Picture", new { Id = db.Pictures.Find(id).FolderId });
        }

    // POST: Votes/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
    //[HttpPost]
    //    [ValidateAntiForgeryToken]
    //    public ActionResult Create([Bind(Include = "Id,PictureId")] Vote vote, int pictureId)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            vote.PictureId = pictureId;
    //            db.Votes.Add(vote);
    //            db.SaveChanges();
    //            return RedirectToAction("Index", "Folder");
    //        }

    //        ViewBag.PictureId = new SelectList(db.Pictures, "Id", "Title", vote.PictureId);
    //        //ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", vote.UserId);
    //        return View(vote);
    //    }

        // GET: Votes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vote vote = db.Votes.Find(id);
            if (vote == null)
            {
                return HttpNotFound();
            }
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "Title", vote.PictureId);
            //ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", vote.UserId);
            return View(vote);
        }

        // POST: Votes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PictureId")] Vote vote)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vote).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.Pictures, "Id", "Title", vote.PictureId);
            //ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", vote.UserId);
            return View(vote);
        }

        // GET: Votes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vote vote = db.Votes.Find(id);
            if (vote == null)
            {
                return HttpNotFound();
            }
            return View(vote);
        }

        // POST: Votes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vote vote = db.Votes.Find(id);
            db.Votes.Remove(vote);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
