﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Gallery.Models;
using Gallery.CustomFilters;
using Microsoft.AspNet.Identity;


namespace Gallery.Controllers
{
    public class FolderController : Controller
    {
        private GalleryContext db = new GalleryContext();


        // GET: Folder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Folder folder = db.Folders.Find(id);
            if (folder == null)
            {
                return HttpNotFound();
            }
            return View(folder);
        }

        // GET: Folder/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Folder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id,Title,Description,DateAdded,AddedFolder")] Folder folder)
        {
            if (ModelState.IsValid)
            {
                folder.DateAdded = DateTime.Now;
                db.Folders.Add(folder);
                folder.UserId = User.Identity.GetUserId();
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(folder);
        }

        // GET: Folder/Edit/5
        public ActionResult Edit(int? id)
        {
            Folder folder = db.Folders.Find(id);
            if (User.IsInRole("Admin") || folder.UserId == User.Identity.GetUserId())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
                if (folder == null)
                {
                    return HttpNotFound();
                }
                return View(folder);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        // POST: Folder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,DateAdded,AddedFolder")] Folder folder)
        {
            Folder oldFolder = db.Folders.Find(folder.Id);
            if (User.IsInRole("Admin") || oldFolder.UserId == User.Identity.GetUserId())
            {
                if (ModelState.IsValid)
                {
                    oldFolder.Title = folder.Title;
                    oldFolder.Description = folder.Description;
                    db.Entry(oldFolder).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(folder);
            }
            else
            {
                return RedirectToAction("Index");
            }
    
        }

        // GET: Folder/Delete/5
        public ActionResult Delete(int? id)
        {
            Folder folder = db.Folders.Find(id);
            if (User.IsInRole("Admin") || folder.UserId == User.Identity.GetUserId())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
                if (folder == null)
                {
                    return HttpNotFound();
                }
                return View(folder);
            }
            else
            {
                return RedirectToAction("Index");
            }
   
        }

        // POST: Folder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Folder folder = db.Folders.Find(id);
            db.Folders.Remove(folder);
            List<FolderLike> folderLike = db.FolderLikes.Where(x => x.FolderId == id).ToList();
            db.FolderLikes.RemoveRange(folderLike);
            List<Picture> picture = db.Pictures.Where(x => x.FolderId == id).ToList();
            db.Pictures.RemoveRange(picture);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }




        public ActionResult Index(string sortOrder)
        {

            var folders = db.Folders.Include(x => x.FolderLikes).ToList();

            ViewBag.FolderLikesSortParm = sortOrder == "FolderLikes" ? "folderlikes_desc" : "FolderLikes";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            switch (sortOrder)
            {
                case "folderlikes_desc":
                    folders = folders.OrderByDescending(s => s.FolderLikes.Count).ToList();
                    break;
                case "Date":
                    folders = folders.OrderBy(s => s.DateAdded).ToList();
                    break;
                case "date_desc":
                    folders = folders.OrderByDescending(s => s.DateAdded).ToList();
                    break;
                default:
                    folders = folders.OrderBy(s => s.FolderLikes.Count).ToList();
                    break;
            }
            var folder = db.Pictures.Include(x => x.User).ToList();

            return View(folders);
        }

    }
}
