﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class Folder
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Pealkirja sisestamine on kohustuslik")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Pealkirja pikkus peab olema 3-50 märki")]
        public string Title { get; set; }
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Kirjelduse pikkus peab olema 3-100 märki")]
        public string Description { get; set; }
        [Display(Name = "Date added")]
        [DataType(DataType.Date)]
        public DateTime? DateAdded { get; set; }


        public ICollection<Picture> Pictures { get; set; }
        public ICollection<FolderLike> FolderLikes { get; set; }

        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
    }
}