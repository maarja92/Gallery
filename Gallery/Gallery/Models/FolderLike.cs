﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class FolderLike
    {
        public int Id { get; set; }
        public int FolderId { get; set; }
        
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public Folder Folder { get; set; }
    }
}