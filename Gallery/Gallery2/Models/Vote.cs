﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class Vote
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        //public int? FolderId { get; set; }
        public int? PictureId { get; set; }

        public Picture Picture { get; set; }
        public User User { get; set; }
        //public Folder Folder { get; set; }
    }
}