﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class Folder
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Pealkirja sisestamine on kohustuslik")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Pealkirja pikkus peab olema 3-50 märki")]
        public string Title { get; set; }
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Kirjelduse pikkus peab olema 3-100 märki")]
        public string Description { get; set; }
        [Display(Name = "Date added")]
        [DataType(DataType.Date)]
        public DateTime? DateAdded { get; set; }

        [Display(Name = "Folder")]
        public string AddedFolder { get; set; } // Hiljem asendub albumiga
        
        public int? UserId { get; set; }

        public ICollection<Picture> Pictures { get; set; }
        //public ICollection<Vote> Votes { get; set; }
        public User User { get; set; }
    }
}