﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class User
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Kasutajanime sisestamine on kohustuslik!")]
        
        public string UserName { get; set; }

        [Required(ErrorMessage = "Parooli sisestamine on kohustuslik!")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "Parooli pikkus peab olema 5 kuni 10 märki")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "E-maili sisestamine on kohustuslik!")]
        public string Email { get; set; }

        public ICollection<Picture> Pictures { get; set; }
        public ICollection<Folder> Folders { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Vote> Votes { get; set; }



    }
}