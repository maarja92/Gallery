﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class Comment
    {
        public int? Id { get; set; }
        [StringLength(200, MinimumLength = 3, ErrorMessage = "Kirjelduse pikkus peab olema 3-200 märki")]
        public string AddedComment { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateAdded { get; set; }
        public int? UserId { get; set; }
        public int? PictureId { get; set; }

        public Picture Picture { get; set; }
        public User User { get; set; }
    }
}