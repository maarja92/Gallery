﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class Picture
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Pealkirja sisestamine on kohustuslik")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Pealkirja pikkus peab olema 3-50 märki")]
        public string Title { get; set; }
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Kirjelduse pikkus peab olema 3-100 märki")]
        public string Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateAdded { get; set; }

        //public string AddedPicture { get; set; } // Hiljem asendub pildiga
        public int? UserId { get; set; }
        public int? FolderId { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public ICollection<Vote> Votes { get; set; }

        public User User { get; set; }
        public Folder Folder { get; set; }

        public byte[] AddedPicture { get; set; }
    }
}